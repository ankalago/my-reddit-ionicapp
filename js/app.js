(function(){
  var app = angular.module('starter', ['ionic','angularMoment'])

  app.controller('RedditCtrl', function($scope, $http){
    $scope.posts = [];
    $http.get('https://www.reddit.com/r/gaming/new/.json')
      .success(function(posts){
        angular.forEach(posts.data.children, function(post){
          $scope.posts.push(post.data);
        });
      });

    //antiguos posts
    $scope.cargarAntiguosPosts = function(){
      console.log("cargar antiguos");
      var parametros = {};
      if ($scope.posts.length > 0 ) {
        parametros['after'] = $scope.posts[$scope.posts.length - 1].name;
      }
      $http.get('https://www.reddit.com/r/gaming/new/.json', {params : parametros})
      .success(function(posts){
        angular.forEach(posts.data.children, function(post){
          $scope.posts.push(post.data);
        });
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    //nuevos posts
    $scope.cargarNuevosPosts = function(){
      console.log("cargar nuevos");
      var parametros = {};
      if ($scope.posts.length > 0 ) {
        parametros['before'] = $scope.posts[0].name;
      }else{
        return;
      }
      $http.get('https://www.reddit.com/r/gaming/new/.json', {params : parametros})
      .success(function(posts){
        var nuevosPosts = [];
        angular.forEach(posts.data.children, function(post){
          nuevosPosts.push(post.data);
          console.log(nuevosPosts.name);
        });
        $scope.posts = nuevosPosts.concat($scope.posts);
        $scope.$broadcast('scroll.refreshComplete');
      });
    }
  });

  app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
}());